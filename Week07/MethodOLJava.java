class MeOverload{
    int pro(int a, int b){
        return a*b;
    }
    double pro(double a, double b){
        return a*b;
    }
    int pro(int a,int b, int c){
        return a*(b*c);
    }
}
class Main {
    public static void main(String[] args) {
        MeOverload m = new MeOverload();
        System.out.println("The product = "+m.pro(1,26));
        System.out.println("The sum = "+m.pro(2.6,3.7));
        System.out.println("The product = "+m.pro(1,2,30));
    }    
}