#include<iostream>
using namespace std;
class Bird
{
    public:
      virtual void walk()
    {
        cout<<"WALK"<<endl;
    }
    
    virtual void eat()=0;
};
class parrot : public Bird
{
    public:
    
    void eat()
    {
        cout<<"EAT"<<endl;
    }
};
int main()
{
    parrot obj;
    obj.walk();
    obj.eat();
    return 0;
