//Simple Inheritance
class Book
{
    void displayBook()
    {
        System.out.println("Welcome Book");
    }
}
class chapter1 extends Book
{
    void displayCh1()
    {
        System.out.println("Hi Ch1");
    }
}
//Multilevel Inheritance
class chapter2 extends chapter1
{
    void displayCh2()
    {
        System.out.println("Hi Ch2");
    }
}
//Heirarchial Inheritance
class chapter3 extends Book
{
    void displayCh3()
    {
        System.out.println("Hi Ch3");
    }
}
//Already chapter1 Inherited Book
//Hybrid Inheritance
class chapter4 extends chapter2
{
    void displayCh4()
    {
         System.out.println("Hi Ch4");
    }
}
class chapter5 extends chapter2
{
    void displayCh5()
    {
         System.out.println("Hi Ch5");
    }
}
class Main
{
    public static void main(String[] args)
    {
        chapter1 c1 = new chapter1();
        chapter2 c2 = new chapter2();
        chapter3 c3 = new chapter3();
        chapter4 c4 = new chapter4();
        chapter5 c5 = new chapter5();
        System.out.println("Simple Inheritance");
        c1.displayBook();
        c1.displayCh1();
        System.out.println("Multilevel Inheritance");
        c2.displayBook();
        c2.displayCh1();
        c2.displayCh2();
        System.out.println("Hierarchial Inheritance");
        c3.displayBook();
        c3.displayCh3();
        System.out.println("Hybrid Inheritance");
        c4.displayBook();
        c4.displayCh1();
        c4.displayCh2();
        c4.displayCh4();
        c5.displayBook();
        c5.displayCh1();
        c5.displayCh2();
        c5.displayCh5();
    }
}