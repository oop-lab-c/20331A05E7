import java.util.*;

class a {
    void fun() {
        System.out.println("it's fun");
    }
}

class b extends
public a{
    void run(){
        System.out.println("take a quick run");
    }
}

class c extends
private a{
    void walk(){
        System.out.println("i'm on a walk");
    }
}

class d extends
protected a{
    void bark(){
        System.out.println("meow...bowwww");
    }
}
