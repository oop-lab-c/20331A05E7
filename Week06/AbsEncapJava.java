import java.util.*;
class week4 {
    private int priVar;
    public int pubVar;
    protected int proVar;

    void setVar(int priValue, int proValue, int pubValue) {
        priVar = priValue;
        proVar = proValue;
        pubVar = pubValue;
    }

    void getVar() {
        System.out.println(priVar);
        System.out.println(pubVar);
        System.out.println(proVar);
    }

    public static void main(String[] args) {
        week4 obj = new week4();
        obj.setVar(10, 20, 30);
        obj.getVar();
    }

}