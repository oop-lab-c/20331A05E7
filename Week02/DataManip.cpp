//Program to demonstrate the usage of datamanipulators(endl,ends,ws,flush,setw,setfill,setprecision)
#include<iostream>
#include<iomanip>
#include<istream>
#include<sstream>
#include<string>
using namespace std;
int main()
{
    int x=1, y=2;
    float z=3.5678;
    cout << "hello" << endl
     << "test" <<  setw(4) << setfill('*') << x <<  endl
    << "exam" << setw(4) << y << endl
    << setprecision(2) << z << flush <<endl;
    istringstream str("       welcome");
    string line;
    getline(str >> std :: ws, line);
    cout << line << endl;
    cout << "a" ;
    cout << "b" << ends << "/";
}