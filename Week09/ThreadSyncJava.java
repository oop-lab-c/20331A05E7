class Hello{
    synchronized void hello(String name){
        for (int i = 0; i<5; i++){
            System.out.println("Hello"+name);
        }
    }
}
class MyThread  extends Thread{
    Hello z;
    String name;
    MyThread(Hello h , String name){
        z = h;
        this.name = name;
    }
    public void run(){
        z.hello(name);
    }
}
class SynThread{
    public static void main(String[] args) {
        Hello h = new Hello();
        MyThread t1 = new MyThread(h,"MVGR");
        MyThread t2 = new MyThread(h,"CSE");
        t1.start();
        t2.start();

    }
}