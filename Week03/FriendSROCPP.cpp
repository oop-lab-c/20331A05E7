#include<iostream>
using namespace std;

//inline function
inline void displayWelcomeMessage() 
{
    cout<<"Welcome"<<endl;
}

class Box
{
    float length,width,height;
    public:
    //Parameterized constructor to assign the values to the instance variables
    Box(float l,float w,float h)
    {
      length = l;
      width = w;
      height = h;
    }

    //declaration of member funtion outside the class 
    void boxVolume(float l, float w, float h);

    //friend function declaration with friend keyword
    friend void displayBoxDimensions(Box obj); 

    //member function inside the class
    void boxArea(float l, float w)
    {
        float area=l*w;
        cout<<"Area of the Box: "<<area<<" square units"<<endl;
    }
};

//definition of member function outside the class using Scope Resolution Operator
void Box :: boxVolume(float l,float w,float h)
{
    float volume=l*w*h;
    cout<<"volume of the box : "<<volume<<" cubic units"<<endl;
}

//friend function definition
void displayBoxDimensions(Box obj)
{
    cout<<"length : "<<obj.length<<endl;
    cout<<"width : "<<obj.width<<endl;
    cout<<"height : "<<obj.height<<endl;
}

int main()
{
    float length,width,height;
    //Taking the Dimensions of box as input from the user
    cout<<"Enter the dimensions of the Box"<<endl;
    cout<<"Length :"<<endl;
    cin>>length;
    cout<<"Width :"<<endl;
    cin>>width;
    cout<<"Height :"<<endl;
    cin>>height;
    Box obj(length,width,height);
    displayWelcomeMessage();//calling inline function
    displayBoxDimensions(obj);//calling friend function
    obj.boxArea(length,width);//calling member function defined inside the class
    obj.boxVolume(length,width,height);//calling member function defined outside the class
    return 0;
}
