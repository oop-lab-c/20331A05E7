import java.util.Scanner;

class Exception{
    public static void main(String[] args) {
        int x,y;
        System.out.println("Enter the two numbers = ");
        Scanner sc = new Scanner(System.in);
        x = sc.nextInt();
        y = sc.nextInt();
        try{
            double q = x/y;
            System.out.println("The quotient = "+q);
        }
        catch(ArithmeticException e){
            System.out.println(e); // we can use e.toString()
            e.printStackTrace();;   //prints exception name, decriptipn, stackTrace
            System.out.println(e.toString()); //prints exception name, decriptipn
            System.out.println(e.getMessage());//prints exception name
            System.err.println("Value by zero error");
        }
    }
}
